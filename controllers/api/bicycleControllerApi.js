let Bicycle = require('../../models/bicycle');

exports.bicyclesList = async(req, res) => {
  const bicycles = await Bicycle.allBicycles();
  res.status(200).json({
    bicycles
  })
}

exports.bicycleCreate = async (req, res) => {
  let bike = new Bicycle({
    code: req.body.code,
    color: req.body.color,
    model: req.body.model,
    location:[
      req.body.lat,
      req.body.lng
    ]
  });

  await Bicycle.add(bike);

  res.status(200).json({
    bicycle: bike
  })
}

exports.bicycleUpdate = async (req, res) => {
  await Bicycle.findByCode(req.body.code);
  let bike = new Bicycle({
    code: req.body.code,
    color: req.body.color,
    model: req.body.model,
    location:[
      req.body.lat,
      req.body.lng
    ]
  });
  await bike.save();
  res.status(200).json({
    bicycle: bike
  })
}

exports.bicycleDelete = async (req, res) => {
  await Bicycle.removeByCode(req.body.code);

  res.status(204).send();
}
