const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const moment = require("moment");

const reservationSchema = new Schema({
  startDate: Date,
  endDate: Date,
  bicycle: {
    type: Schema.Types.ObjectId,
    ref: "Bicycle"
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: "User"
  }
})

reservationSchema.methods.reservationDays = function () {
  return moment(this.startDate). diff(moment(this.endDate), "days") +1;
}

module.exports = mongoose.model("Reservation", reservationSchema);
