var express = require('express');
var router = express.Router();
var bicycleController = require('../controllers/bicycle');

router.get('/', bicycleController.bicyclesList);
router.get('/create', bicycleController.bicycleCreateGet);
router.post('/create', bicycleController.bicycleCreatePost);
router.get('/:id/update', bicycleController.bicycleUpdateGet);
router.post('/:id/update', bicycleController.bicycleUpdatePost);
router.post('/:id/delete', bicycleController.bicycleDeletePost);

module.exports = router;

