const mongoose = require("mongoose");
const request = require("request");
const Bicycle =  require("../../models/bicycle");
const User = require("../../models/user");
const Reservation =  require("../../models/reservation");

const server = require("../../bin/www");
let baseUrl = "http://localhost:3000/api";

describe("Api usuario", () => {
  beforeAll((done) => { mongoose.connection.close(done) });

	beforeEach((done) => {
		mongoose.disconnect();
		let mongoDB = "mongodb://localhost/testdb";
		mongoose.connect(mongoDB, { useNewUrlParser:true, useUnifiedTopology: true  });

		const db = mongoose.connection;
		db.on('error', console.error.bind(console, 'connection error'));
		db.once('open', () => {
			console.log('We are connected to test database');
			done();
		});
	});

	afterEach((done) => {
    Reservation.deleteMany({}, (err, success) =>  {
      if (err) {
        console.log(err);
      }

      User.deleteMany({},  (err, success) => {
        if (err) {
          console.log(err);
        }

        Bicycle.deleteMany({}, (err, success) => {
          if (err) {
            console.log(err);
          }
          mongoose.connection.close(done);
        });
      });
    });
  });

  describe("GET Users", () => {
    it("Status 200", (done) => {
      request.get(`${baseUrl}/users`, (err, response, body) => {
        const result = JSON.parse(body);
        expect(response.statusCode).toBe(200);
        expect(result.users.length).toBe(0);
        done();
      })
    })
  })

  describe('POST Users /create', () => {
    it('Status 200', (done) => {
      let headers = {'Content-Type' : 'application/json'}
      let aUser = '{ "name": "John Doe" }';

      request.post({
        headers: headers,
        url: `${baseUrl}/users/create`,
        body: aUser
        }, (error, response, body) => {
          expect(response.statusCode).toBe(200);
          const result = JSON.parse(body).user;
          expect(result.name).toBe("John Doe");
          done();
      })
    })
  })

  describe("POST Users /reserve", () => {
    it("Status 200", (done) => {
      let headers = {'Content-Type' : 'application/json'}
      let User = '{ "name": "John Doe" }';

      request.post(
        {
          headers: headers,
          url: `${baseUrl}/users/create`,
          body: User
        }, (error, response, body) => {
          expect(response.statusCode).toBe(200);
          const userId = JSON.parse(body).user._id;
          const aBicycle = '{"code": 2, "color": "Rojo", "model": "Ruta", "lat": -33.4655, "lng": -70.6122}';

          request.post(
            {
              headers: headers,
              url: `${baseUrl}/bicycles/create`,
              body: aBicycle,
            },(error, response, body) => {
              expect(response.statusCode).toBe(200);
              const aReserve = '{ "_id": "' + userId + '", "startDate":"2020/10/04", "endDate":"2020/10/05"}';
              request.post(
                {
                  headers: headers,
                  url: `${baseUrl}/users/reserve`,
                  body: aReserve                  
                }, (error, response, body) => {                  
                  expect(response.statusCode).toBe(200);
                  const result = JSON.parse(body).reservation;
                  expect(result.startDate).toBe("2020/10/04");
                  expect(result.endDate).toBe("2020/10/05");
                  done();
                }
              );
            }
          );
        }
      );
    });
  });
})

